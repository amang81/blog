class Comment < ActiveRecord::Base
  belongs_to :post
  # validation required to link comments with post
  validates_presence_of :post_id
  validates_presence_of :body
end
